var app = angular.module("Music",[]);
var baseUrl = location.origin;
app.directive('ngFiles', ['$parse', function ($parse) {
	function fn_link(scope, element, attrs) {
         var onChange = $parse(attrs.ngFiles);
         element.on('change', function (event) {
             onChange(scope, { $files: event.target.files });
         });
     };

     return {
         link: fn_link
     }
 } ]);
app.controller("MusicController", function($scope, $http){
	$scope.action = 'add';
	$scope.data = {};
	$scope.add = function(){
		switch ($scope.action) {
			case 'add':
				$scope.addData();
			break;
			case 'edit':
				$scope.edit();
			break;
			case 'delete':
				$scope.delete();
			break;
		}
	};

		$scope.addData = function(){
		$http.post(
			'index.php?r=music/add',
			{
				data: $scope.data
			}
		).then((response) => {
			$scope.clearData();
			$scope.getAllMusic();
		}, (response) => {
			console.log('error');
		});
	};

	$scope.getAllMusic = function(){
		$http.get(
			'index.php?r=music/view-all'
		).then((response) => {
			$scope.viewAllMusic = response.data;
			$scope.viewAllMusic2 = response.data;
			for(var i=0;i < $scope.viewAllMusic2.length;i++){
				$scope.viewAllMusic2[i].path = baseUrl + '/frontend/web/' + response.data[i].path + response.data[i].hash + '.' + response.data[i].extension;
			}
			console.log($scope.viewAllMusic2);
		}, (response) => {
		});
	};

	$scope.clearData = function(){
		$scope.data = {
			hash: '',
			path: '',
			name: '',
			extantion: '',
		}
	};

	$scope.beforeEdit = function(music){
		$scope.data = music;
		$scope.action = 'edit';
	};

	$scope.edit = function(){
		$http.post(
			'index.php?r=music/edit-music',
			{
				data: $scope.data
			}
		).then((responce)=> {
			$scope.clearData();
			$scope.getAllMusic();
			$scope.action = 'add';
		}, (responce) => {
			alert('Somsing was wrong!');
		});
	};

	$scope.delete = function(id){
		$scope.action = 'delete';
		$http.post(
			'index.php?r=music/delete-music',
			{
				id: id
			}
		).then((responce)=> {
			$scope.getAllMusic();
			$scope.action = 'add';
		}, (responce) => {
			alert('Somsing was wrong!');
		});

	}

	$scope.getAllMusic();


   var formdata = new FormData();
   $scope.getTheFiles = function ($files) {
       angular.forEach($files, function (value, key) {
           formdata.append(key, value);
       });
   };
   // NOW UPLOAD THE FILES.
   $scope.uploadFiles = function () {
   	 var request = {
           method: 'POST',
           url: 'index.php?r=music/upload',
           data: formdata,
           headers: {
               'Content-Type': undefined
           }
       };

       // SEND THE FILES.
       $http(request).then((responce) => {
					$scope.getAllMusic();
					$scope.action = 'add';
					alert('Successfully added!');
       }, (responce) => {

				alert('Somsing was wrong!');
       });
     }

});