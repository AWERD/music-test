<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MusicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Musics');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="music-index">

<!--     <h1><?= Html::encode($this->title) ?></h1>
<?php Pjax::begin(); ?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<p>
    <?= Html::a(Yii::t('app', 'Create Music'), ['create'], ['class' => 'btn btn-success']) ?>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'hash',
        'path',
        'name',

        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
<?php Pjax::end(); ?> -->
</div>
<div ng-app="Music">
    <div ng-controller="MusicController">
        <fieldset>
            <legend>Music data</legend>
            <table class="table">
                <thead>
                    <th>#</th>
                    <th style="display: none;">ID</th>
                    <th>Hash</th>
                    <th>Path</th>
                    <th>Name</th>
                    <th>Extension</th>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td style="display: none;">
                            <!-- <input type="text" class="form-control" ng-model="data.id"> -->
                        </td>
                        <td>
                            <input type="text" class="form-control" ng-model="data.hash">
                        </td>
                        <td>
                            <input type="text" class="form-control" ng-model="data.path">
                        </td>
                        <td>
                            <input type="text" class="form-control" ng-model="data.name">
                        </td>
                        <td>
                            <input type="text" class="form-control" ng-model="data.extantion">
                        </td>
                        <td>
                            <button class="btn btn-primary" ng-click="add()">Add</button>
                        </td>
                    </tr>
                    <tr ng-repeat="music in viewAllMusic">
                        <td>{{$index+1}}</td>
                        <td style="display: none;">{{music.id}}</td>
                        <td>{{music.hash}}</td>
                        <td>{{music.path}}</td>
                        <td>{{music.name}}</td>
                        <td>{{music.extension}}</td>
                        <td>
                            <button class="btn btn-success" ng-click="beforeEdit(music)">Edit</button>
                            <button class="btn btn-danger" ng-click="delete(music.id)">Delete</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>

        <input type="file"  accept="audio/*" id="file1" name="file" multiple
            ng-files="getTheFiles($files)" />

        <input type="button" ng-click="uploadFiles()" value="Upload" />

        <ul ng-repeat="musics in viewAllMusic2">
            <li>
                <p>{{musics.name}}</p>
                <audio controls>
                  <source src='{{musics.path}}' ng-attribute-type="{{'audio/' + musics.extension}}">
                  Тег audio не поддерживается вашим браузером. 
                </audio>
            </li>
        </ul>
    </div>

</div>
