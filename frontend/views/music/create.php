<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Music */

$this->title = Yii::t('app', 'Create Music');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Musics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="music-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
