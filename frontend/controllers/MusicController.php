<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use frontend\models\Music;
use frontend\models\MusicSearch;

/**
 * MusicController implements the CRUD actions for Music model.
 */
class MusicController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Music models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MusicSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Music model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Music model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Music();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Music model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Music model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Music model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Music the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Music::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function beforeAction($action)
    {
       $this->enableCsrfValidation = false;
       return parent::beforeAction($action); 
    }

    public function actionAdd() 
    {
        $postData = file_get_contents('php://input');
        $dataObj = json_decode($postData);

        $music = new Music();
        $music->hash = $dataObj->data->hash;
        $music->path = $dataObj->data->path;
        $music->name = $dataObj->data->name;
        $music->extension = $dataObj->data->extension;
        $music->save(false);
    }

    public function actionViewAll()
    {
        $music = Music::find()->asArray()->all();
        echo json_encode($music);
        die;
    }

    public function actionEditMusic()
    {
        $postData = file_get_contents('php://input');
        $dataObj = json_decode($postData);
        $music = Music::findOne($dataObj->data->id);
        // $music->id = $dataObj->data->id;
        $music->hash = $dataObj->data->hash;
        $music->path = $dataObj->data->path;
        $music->name = $dataObj->data->name;
        $music->extension = $dataObj->data->extension;
        $music->save(false);
    }

    public function actionDeleteMusic()
    {
        $postData = file_get_contents('php://input');
        $dataObj = json_decode($postData);
        $music = Music::findOne($dataObj->id);
        $music->delete();
    }

    public function actionUpload() {
        $i=0;
        $target_dir = "uploads/";
        $uploadOk = 1;
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        } else {
            $files = $_FILES;
            for ($i=0; $i < count($files); $i++) { 
                $path = $files[$i]['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                if(move_uploaded_file($files[$i]["tmp_name"], $target_dir . basename(md5($files[$i]['name']). "." .$ext)));
                $model = new Music();
                $model->hash = md5($files[$i]['name']);
                $model->path = 'uploads/';
                $model->name = $files[$i]["name"];
                $model->extension = $ext;
                $model->save(false);
            }
            echo json_encode($files[0]['tmp_name']);die;
        }
    }
}