<?php

use yii\db\Migration;

/**
 * Handles adding extension to table `music`.
 */
class m180223_134047_add_extension_column_to_music_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('music', 'extension', $this->string(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('music', 'extension');
    }
}
