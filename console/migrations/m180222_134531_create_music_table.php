<?php

use yii\db\Migration;

/**
 * Handles the creation of table `music`.
 */
class m180222_134531_create_music_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('music', [
            'id' => $this->primaryKey(),
            'id' => $this->integer(11)->notNull()->unique(),
            'hash' => $this->string(11),
            'path' => $this->string(11),
            'name' => $this->string(11),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('music');
    }
}
